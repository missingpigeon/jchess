package com.jchess.view;

import com.jchess.logic.AI.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PromotionWindow extends JFrame {
    private JLabel lblTarget;
    private JButton btnAccept;
    private JButton btnCancel;
    private JComboBox cmbTarget;
    private BoardView board;

    public PromotionWindow(BoardView board) {
        this.board = board;
        JPanel detailsPanel = new JPanel(new FlowLayout());
        cmbTarget = new JComboBox();
        cmbTarget.addItem("Rook");
        cmbTarget.addItem("Knight");
        cmbTarget.addItem("Bishop");
        cmbTarget.addItem("Queen");
        cmbTarget.setSelectedIndex(3);
        lblTarget = new JLabel("Promote to:", JLabel.TRAILING);
        btnAccept = new JButton("Accept");
        btnCancel = new JButton("Cancel");
        detailsPanel.add(lblTarget);
        lblTarget.setLabelFor(cmbTarget);
        detailsPanel.add(cmbTarget);
        detailsPanel.add(btnAccept);
        detailsPanel.add(btnCancel);
        setContentPane(detailsPanel);
        btnAccept.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Piece promotePiece;
                if (board.getBlackPlayer().isCurrentPlayer()) {
                    switch(cmbTarget.getSelectedIndex()) {
                        case 0:
                            promotePiece = new Rook(Piece.Side.black);
                            break;
                        case 1:
                            promotePiece = new Knight(Piece.Side.black);
                            break;
                        case 2:
                            promotePiece = new Bishop(Piece.Side.black);
                        case 3:
                            promotePiece = new Queen(Piece.Side.black);
                            break;
                        default: promotePiece = new Queen(Piece.Side.black);

                    }
                    board.getBlackPlayer().setPromotePiece(promotePiece);
                }
                else {
                    switch(cmbTarget.getSelectedIndex()) {
                        case 0:
                            promotePiece = new Rook(Piece.Side.white);
                            break;
                        case 1:
                            promotePiece = new Knight(Piece.Side.white);
                            break;
                        case 2:
                            promotePiece = new Bishop(Piece.Side.white);
                        case 3:
                            promotePiece = new Queen(Piece.Side.white);
                            break;
                        default: promotePiece = new Queen(Piece.Side.white);

                    }
                    board.getWhitePlayer().setPromotePiece(promotePiece);
                }
            }
        });
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
}
