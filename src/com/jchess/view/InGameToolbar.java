package com.jchess.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class InGameToolbar extends JPanel {
    private Image background;
    private Image btnNewGame;
    private Image btnUndo;
    private Image btnSettings;
    private Image btnMainMenu;
    private Image btnNewGameHover;
    private Image btnUndoHover;
    private Image btnSettingsHover;
    private Image btnMainMenuHover;
    private boolean isHovering;
    private Point mousePosition;
    private MainWindow parent;
    private BoardView board;

    public InGameToolbar(MainWindow parent, BoardView board) {
        this.parent = parent;
        setPreferredSize(new Dimension(1030, 32));
        this.board = board;
        Toolkit resourceKit = Toolkit.getDefaultToolkit();
        background = resourceKit.getImage("images/main_toolbar.png");
        btnNewGame = resourceKit.getImage("images/toolbar_btn_newgame.png");
        btnUndo = resourceKit.getImage("images/toolbar_btn_undo.png");
        btnSettings = resourceKit.getImage("images/toolbar_btn_settings.png");
        btnMainMenu = resourceKit.getImage("images/toolbar_btn_menu.png");
        btnNewGameHover = resourceKit.getImage("images/toolbar_btn_newgame_hover.png");
        btnUndoHover = resourceKit.getImage("images/toolbar_btn_undo_hover.png");
        btnSettingsHover = resourceKit.getImage("images/toolbar_btn_settings_hover.png");
        btnMainMenuHover = resourceKit.getImage("images/toolbar_btn_menu_hover.png");
        this.addMouseListener(new MouseHandler());
        this.addMouseMotionListener(new MouseMovementHandler());
    }

    private class MouseMovementHandler extends MouseMotionAdapter {
        public void mouseMoved(MouseEvent event) {
            mousePosition = event.getPoint();
            Point mousePositionRelativeToScreen = new java.awt.Point(event.getLocationOnScreen());
            SwingUtilities.convertPointFromScreen(mousePositionRelativeToScreen, event.getComponent());
            isHovering = (mousePosition.getX() >= 480 && mousePosition.getX() <= 1030
                    && mousePosition.getY() >= 0 && mousePosition.getY() <= 32);
            repaint();
        }
    }

    private class MouseHandler extends MouseAdapter {
        public void mouseExited(MouseEvent event) {
            isHovering = false;
            repaint();
        }
        public void mouseClicked(MouseEvent event) {
            mousePosition = event.getPoint();
            if (mousePosition.getX() < 890 && mousePosition.getX() > 740) {
                PromotionWindow promoteWindow = new PromotionWindow(board);
                promoteWindow.setResizable(false);
                promoteWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                promoteWindow.pack();
                promoteWindow.setVisible(true);
                promoteWindow.setLocationRelativeTo(null);
            }
        }
    }

    public void paintComponent(Graphics context) {
        Graphics2D painter = (Graphics2D) context;
        painter.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        painter.drawImage(background, 0, 0, this);
        painter.drawImage(btnNewGame, 460, 0, this);
        painter.drawImage(btnUndo, 600, 0, this);
        painter.drawImage(btnSettings, 740, 0, this);
        painter.drawImage(btnMainMenu, 890, 0, this);
        if (isHovering) {
            if (mousePosition.getX() < 600 && mousePosition.getX() > 460)
                painter.drawImage(btnNewGameHover, 460, 0, this);
            if (mousePosition.getX() < 740 && mousePosition.getX() > 600)
                painter.drawImage(btnUndoHover, 600, 0, this);
            if (mousePosition.getX() < 890 && mousePosition.getX() > 740)
                painter.drawImage(btnSettingsHover, 740, 0, this);
            if (mousePosition.getX() > 890)
                painter.drawImage(btnMainMenuHover, 890, 0, this);
        }
    }
}
