package com.jchess.view;

import java.awt.*;
import javax.swing.*;
import com.jchess.logic.AI.Piece;
import com.jchess.logic.shared.Game;

public class MainWindow extends JFrame {
    private InGameToolbar toolbar;
    private BoardView board;
    private MainMenu menu;
    private PlayerView blackPlayerPane;
    private PlayerView whitePlayerPane;
    private String IP;
    private String Port;

    public MainWindow() {
        super("JChess");
        Toolkit resourceKit = Toolkit.getDefaultToolkit();
        Image frameIcon = resourceKit.getImage("images/chess_icon.png");
        this.setIconImage(frameIcon);
        setResizable(false);
        menu = new MainMenu(this);
        getContentPane().add(menu, BorderLayout.CENTER);
    }

    public void newSingleplayerGame() {
        board = new BoardView(this, Game.GAMEMODE_SINGLEPLAYER);
        initializeGame();
    }

    public void newMultiplayerGame() {
        board = new BoardView(this, Game.GAMEMODE_MULTIPLAYER);
        initializeGame();
    }

    public void newServerGame() {
        board = new BoardView(this, Game.GAME_SERVER);
        initializeGame();
    }

    public void newClientGame() {
        board = new BoardView(this, Game.GAME_CLIENT);
        initializeGame();
    }

    public void initializeGame() {
        blackPlayerPane = new PlayerView(Piece.Side.black, this, board);
        whitePlayerPane = new PlayerView(Piece.Side.white, this, board);
        toolbar = new InGameToolbar(this, board);
        getContentPane().remove(menu);
        getContentPane().add(toolbar, BorderLayout.BEFORE_FIRST_LINE);
        getContentPane().add(blackPlayerPane, BorderLayout.WEST);
        getContentPane().add(board, BorderLayout.CENTER);
        getContentPane().add(whitePlayerPane, BorderLayout.EAST);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        MainWindow frame = new MainWindow();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }

    public void updateSidePanels() {
        blackPlayerPane.validate();
        blackPlayerPane.repaint();
        whitePlayerPane.validate();
        whitePlayerPane.repaint();
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getPort() {
        return Port;
    }

    public void setPort(String port) {
        Port = port;
    }
}
