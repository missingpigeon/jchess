package com.jchess.view;

import com.jchess.logic.AI.Board;
import com.jchess.logic.AI.Piece;
import com.jchess.logic.shared.Game;
import com.jchess.logic.shared.Player;

import java.awt.*;

import javax.swing.JPanel;

public class PlayerView extends JPanel {
    Piece.Side side;
    private Image header;
    private Image header_turn;
    private Image background;
    private Image headerShadow;
    private Image captured;
    private Player currentPlayer;
    private Font moveCountFont;
    private Font timeElapsedFont;
    private Font capturedFont;
    private Font captionFont;
    private BoardView board;

    public PlayerView(Piece.Side playerside, MainWindow parent, BoardView board) {
        side = playerside;
        setLayout(null);
        setPreferredSize(new Dimension(200, 630));
        this.board = board;
        Toolkit resourceKit = Toolkit.getDefaultToolkit();
        if (side == Piece.Side.black) {
            header = resourceKit.getImage("images/black_header.png");
            header_turn = resourceKit.getImage("images/black_header_turn.png");
            background = resourceKit.getImage("images/sidepane_left_background.png");
            captured = resourceKit.getImage("images/black_captured.png");
        } else {
            header = resourceKit.getImage("images/white_header.png");
            header_turn = resourceKit.getImage("images/white_header_turn.png");
            background = resourceKit.getImage("images/sidepane_right_background.png");
            captured = resourceKit.getImage("images/white_captured.png");
        }
        headerShadow = resourceKit.getImage("images/header_shadow.png");
        moveCountFont = new Font("Segoe UI", Font.BOLD, 72);
        timeElapsedFont = new Font("Segoe UI", Font.BOLD, 18);
        capturedFont = new Font("Segoe UI", Font.PLAIN, 18);
        captionFont = new Font("Segoe UI", Font.PLAIN, 10);
    }

    public void paintComponent(Graphics context) {
        if (side == Piece.Side.black) {
            currentPlayer = board.getBlackPlayer();
        } else {
            currentPlayer = board.getWhitePlayer();
        }
        super.paintComponent(context);
        Graphics2D painter = (Graphics2D) context;
        painter.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        setBackground(new Color(20, 20, 20));
        painter.drawImage(background, 0, 0, this);
        if (currentPlayer.isCurrentPlayer())
            painter.drawImage(header_turn, 0, 0, this);
        else painter.drawImage(header, 0, 0, this);
        painter.drawImage(headerShadow, 0, 81, this);
        painter.setColor(new Color(140, 140, 140));
        painter.setFont(moveCountFont);
        painter.drawString(String.format("%02d",
                currentPlayer.getMovesMade()), 60, 160);
        painter.setFont(captionFont);
        painter.drawString("MOVES MADE", 68, 190);
        painter.setFont(timeElapsedFont);
        painter.drawString(getTimeElapsed(currentPlayer.getTotalTimeTaken()), 64, 236);
        painter.setFont(captionFont);
        painter.drawString("TIME ELAPSED", 66, 260);
        painter.drawImage(captured, 0, 280, this);
        painter.drawString("CAPTURED PIECES", 64, 550);
        painter.setFont(capturedFont);
        painter.drawString("0", 72, 352);
        painter.drawString("0", 148, 352);
        painter.drawString("0", 72, 426);
        painter.drawString("0", 148, 426);
        painter.drawString("0", 72, 502);
    }

    private String getTimeElapsed(int time) {
        int HH = time / 3600;
        int ss = (time - HH * 3600) % 60;
        int mm = (time - HH * 3600) / 60;
        return String.format("%02d:%02d:%02d", HH, mm, ss);
    }
}
