package com.jchess.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MultiplayerWindow extends JFrame {
    private JLabel lblAddress;
    private JLabel lblPort;
    private JLabel lblType;
    private JButton btnStart;
    private JButton btnCancel;
    private JTextField txtAddress;
    private JTextField txtPort;
    private JComboBox cmbType;
    MainWindow parent;

    public MultiplayerWindow(MainWindow parent) {
        this.parent = parent;
        JPanel detailsPanel = new JPanel(new FlowLayout());
        cmbType = new JComboBox();
        cmbType.addItem("Server");
        cmbType.addItem("Client");
        cmbType.addItem("Local");
        lblAddress = new JLabel("Address:", JLabel.TRAILING);
        lblPort = new JLabel("Port:", JLabel.TRAILING);
        lblType = new JLabel("Connection type:", JLabel.TRAILING);
        txtAddress = new JTextField("", 20);
        txtPort = new JTextField("", 10);
        btnStart = new JButton("Start");
        btnCancel = new JButton("Cancel");
        btnStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                parent.setIP(txtAddress.getText());
                parent.setPort(txtPort.getText());
                if (cmbType.getSelectedIndex() == 0) parent.newServerGame();
                else if (cmbType.getSelectedIndex() == 1) parent.newClientGame();
                else if (cmbType.getSelectedIndex() == 2) parent.newMultiplayerGame();
            }
        });
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        detailsPanel.add(lblAddress);
        lblAddress.setLabelFor(txtAddress);
        detailsPanel.add(txtAddress);
        detailsPanel.add(lblPort);
        lblPort.setLabelFor(txtPort);
        detailsPanel.add(txtPort);
        detailsPanel.add(lblType);
        lblType.setLabelFor(cmbType);
        detailsPanel.add(cmbType);
        detailsPanel.add(btnStart);
        detailsPanel.add(btnCancel);
        setContentPane(detailsPanel);
    }
}
