package com.jchess.view;

import java.awt.*;

import java.awt.event.*;

import javax.swing.*;

public class MainMenu extends JPanel {
    private MainWindow mainWindow;
    private boolean isHighlighted;
    private final int MENU_NEWGAME = 0;
    private final int MENU_NEWMULTIGAME = 1;
    private final int MENU_CREDITS = 2;
    private final int MENU_EXIT = 3;
    private Toolkit resourceKit = Toolkit.getDefaultToolkit();
    private Image background;
    private Image btnNewGame;
    private Image btnNewMultiGame;
    private Image btnCredits;
    private Image btnExit;
    private Image btnNewGameHover;
    private Image btnNewMultiGameHover;
    private Image btnCreditsHover;
    private Image btnExitHover;
    private int highlightedItem;


    public MainMenu(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        setPreferredSize(new Dimension(1030, 630));
        this.addMouseListener(new MouseHandler());
        background = resourceKit.getImage("images/main_menu_background.png");
        btnNewGame = resourceKit.getImage("images/menu_btn_newgame.png");
        btnNewMultiGame = resourceKit.getImage("images/menu_btn_newmultigame.png");
        btnCredits = resourceKit.getImage("images/menu_btn_credits.png");
        btnExit = resourceKit.getImage("images/menu_btn_exit.png");
        btnNewGameHover = resourceKit.getImage("images/menu_btn_newgame_hover.png");
        btnNewMultiGameHover = resourceKit.getImage("images/menu_btn_newmultigame_hover.png");
        btnCreditsHover = resourceKit.getImage("images/menu_btn_credits_hover.png");
        btnExitHover = resourceKit.getImage("images/menu_btn_exit_hover.png");
        isHighlighted = false;
        this.addMouseMotionListener(new MouseMovementHandler());
        this.addMouseListener(new MouseHandler());
    }


    public void paintComponent(Graphics context) {
        super.paintComponent(context);
        Graphics2D painter = (Graphics2D) context;
        painter.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        painter.drawImage(background, 0, 0, this);
        painter.drawImage(btnNewGame, 670, 260, this);
        painter.drawImage(btnNewMultiGame, 670, 340, this);
        painter.drawImage(btnCredits, 670, 420, this);
        painter.drawImage(btnExit, 670, 500, this);
        if (isHighlighted) {
            if (highlightedItem == MENU_NEWGAME) painter.drawImage(btnNewGameHover, 670, 260, this);
            if (highlightedItem == MENU_NEWMULTIGAME) painter.drawImage(btnNewMultiGameHover, 670, 340, this);
            if (highlightedItem == MENU_CREDITS) painter.drawImage(btnCreditsHover, 670, 420, this);
            if (highlightedItem == MENU_EXIT) painter.drawImage(btnExitHover, 670, 500, this);
        }

    }

    private class MouseMovementHandler extends MouseMotionAdapter {
        public void mouseMoved(MouseEvent event) {
            Point mousePosition = event.getPoint();
            if (mousePosition.getX() > 670 && mousePosition.getY() > 260 && mousePosition.getY() < 320) {
                isHighlighted = true;
                highlightedItem = MENU_NEWGAME;
            } else if (mousePosition.getX() > 670 && mousePosition.getY() > 340 && mousePosition.getY() < 400) {
                isHighlighted = true;
                highlightedItem = MENU_NEWMULTIGAME;
            } else if (mousePosition.getX() > 670 && mousePosition.getY() > 420 && mousePosition.getY() < 480) {
                isHighlighted = true;
                highlightedItem = MENU_CREDITS;
            } else if (mousePosition.getX() > 670 && mousePosition.getY() > 500 && mousePosition.getY() < 560) {
                isHighlighted = true;
                highlightedItem = MENU_EXIT;
            } else isHighlighted = false;
            repaint();
        }
    }

    private class MouseHandler extends MouseAdapter {
        public void mouseClicked(MouseEvent event) {
            Point mousePosition = event.getPoint();
            if (mousePosition.getX() > 670 && mousePosition.getY() > 260 && mousePosition.getY() < 320) {
                mainWindow.newSingleplayerGame();
            } else if (mousePosition.getY() > 340 && mousePosition.getY() < 400 && mousePosition.getX() > 670) {
                MultiplayerWindow configWindow = new MultiplayerWindow(mainWindow);
                configWindow.setResizable(false);
                configWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                configWindow.pack();
                configWindow.setVisible(true);
                configWindow.setLocationRelativeTo(null);
            } else if (mousePosition.getY() < 480 && mousePosition.getY() > 420 && mousePosition.getX() > 670) {
                isHighlighted = true;
                JOptionPane.showMessageDialog(mainWindow,
                        "1412075 Nguyễn Thanh Danh: AI & game logic\n" +
                                "1412093 Phạm Minh Quang Duy: User interface\n" +
                                "1412079 Đặng Nhật Duy: Network\n",
                        "Credits",
                        JOptionPane.INFORMATION_MESSAGE);
                highlightedItem = MENU_CREDITS;
            } else if (mousePosition.getY() < 560 && mousePosition.getY() > 500 && mousePosition.getX() > 670) {
                isHighlighted = true;
                highlightedItem = MENU_EXIT;
                mainWindow.dispose();
            }
        }
    }

}
