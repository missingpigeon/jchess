package com.jchess.view;

import javax.swing.*;

import java.awt.event.*;

import com.jchess.logic.shared.Game;
import com.jchess.logic.shared.Move;
import com.jchess.logic.shared.Player;
import com.jchess.logic.shared.Position;
import com.jchess.logic.AI.Board;
import com.jchess.logic.AI.Piece;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.awt.*;

public class BoardView extends JPanel {
    public final Dimension BOARD_DIMENSION =
            new Dimension(630, 630);
    private final int CELL_DIMENSION = 70;
    private final int CELL_PADDING = 15;
    private final Image rIcon;
    private final Image bIcon;
    private final Image qIcon;
    private final Image kIcon;
    private final Image nIcon;
    private final Image pIcon;
    private final Image RIcon;
    private final Image BIcon;
    private final Image QIcon;
    private final Image KIcon;
    private final Image NIcon;
    private final Image PIcon;
    private final Image pathHighlight;
    private final Image hoverHighlight;
    private final Image selectedHighlight;
    private final Image background;
    private Game game;
    private ArrayList<Move> legalMoves;
    private MainWindow parentWindow;
    private Piece.Side winner;
    private int draw;
    private int gameMode;
    private Piece selectedPiece;
    private Piece highlightedPiece;
    private int selectedRow;
    private int selectedCol;
    private int highlightedCol;
    private int highlightedRow;


    private boolean Is_Server = false;
    private boolean Is_Client = false;
    private boolean Game_Started = false;
    private JButton startServer;
    private JButton startClient;
    private boolean Is_Local = false;
    private String Ip_Address;
    private String Port_Number;
    private String Mess;
    private short player_turn = 1;
    private BufferedReader in;
    private PrintWriter out;
    private String code;
    private int newX;
    private int newY;
    private ServerSocket ServerSock;
    private Socket Sock;

    private Recv_Thread Recv;


    public void Start_Server (String Ip, String Port, int gameMode) {

        game = new Game(gameMode);
        this.gameMode = gameMode;
        winner = null;

        Recv = new Recv_Thread();

        Ip_Address = Ip;
        Port_Number = Port;
        player_turn = 1;
        Is_Local = false;
        game.setLastMoved(Piece.Side.black);

        startServer = new JButton("Start Server");
        // Set size and location of StartServer button (random) =))~
        startServer.setSize(150,25);
        startServer.setLocation(200,300);

        startServer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {

                    ServerSock = new ServerSocket(Integer.parseInt(Port_Number));

                    Thread Server = new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {

                                Sock = ServerSock.accept();

                                in = new BufferedReader(new InputStreamReader(Sock.getInputStream()));
                                out = new PrintWriter(Sock.getOutputStream());

                                startServer.setVisible(false);
                                startServer = null;

                                Recv.start();

                                Game_Started = true;
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }

                    });

                    Server.start();

                } catch (IOException e1) {
                    e1.printStackTrace();
                    // ERROR
                }
                startServer.setText("Waiting for client to connect...");

            }

        });
        Is_Local = false;
        add(startServer);

        Is_Server = true;
        repaint();
    }


    public void Start_Client(String Ip, String Port, int gameMode) {

        game = new Game(gameMode);
        this.gameMode = gameMode;
        winner = null;

        Recv = new Recv_Thread();

        Ip_Address = Ip;
        Port_Number = Port;
        Is_Local = false;
        player_turn = 2;
        game.setLastMoved(Piece.Side.white);

        startClient = new JButton("Start Client");
        // Set feature of StartClient button
        startClient.setSize(150,25);
        startClient.setLocation(200,300);

        startClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {

                    Sock = new Socket(Ip_Address, Integer.parseInt(Port_Number));

                    in = new BufferedReader(new InputStreamReader(Sock.getInputStream()));
                    out = new PrintWriter(Sock.getOutputStream());

                    Recv.start();


                } catch (UnknownHostException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                    JOptionPane.showConfirmDialog(null,"Client error!","Error",JOptionPane.ERROR_MESSAGE);
                }
                startClient.setVisible(false);
                startClient = null;
            }
        });
        Is_Client = true;
        add(startClient);

        repaint();
    }


    public void Send_Mess() {
        out.print(Mess);
        out.print("\r\n");
        out.flush();
    }


    class Recv_Thread extends Thread {

        public synchronized void run () {

            while (true) {

                try {
                    Mess = in.readLine();

                } catch (IOException ex) {
                    ex.printStackTrace();
                    System.out.println("ERROR!");
                }

                if (Mess != null) {

                    code = Mess.substring(0,3);
                    newX = Integer.parseInt(Mess.substring(3,4));
                    newY = Integer.parseInt(Mess.substring(4,5));

                    if (player_turn == 1) {

                        Position cur_pos = game.getPosition(code);
                        Move move = new Move(cur_pos,new Position(newX, newY),null);
                        game.makeMove(move);

                        checkForVictory();
                    } else {

                        Position cur_pos = game.getPosition(code);
                        Move move = new Move(cur_pos,new Position(newX, newY),null);
                        game.makeMove(move);

                        checkForVictory();
                    }
                    repaint();
                }
            }
        }

    }


    public BoardView(MainWindow parent, int gameMode) {
        setPreferredSize(BOARD_DIMENSION);
        parentWindow = parent;
        if (gameMode == Game.GAME_SERVER) {
            Start_Server(parentWindow.getIP(), parentWindow.getPort(), gameMode);
        } else if (gameMode == Game.GAME_CLIENT) {
            Start_Client(parentWindow.getIP(), parentWindow.getPort(), gameMode);
        } else {
            newGame(gameMode);
        }
        Toolkit resourceKit = Toolkit.getDefaultToolkit();
        background = resourceKit.getImage("images/board_background.png");
        pathHighlight = resourceKit.getImage("images/highlight_path.png");
        hoverHighlight = resourceKit.getImage("images/highlight_hover.png");
        selectedHighlight = resourceKit.getImage("images/highlight_selected.png");
        rIcon = resourceKit.getImage("images/br.gif");
        bIcon = resourceKit.getImage("images/bb.gif");
        qIcon = resourceKit.getImage("images/bq.gif");
        kIcon = resourceKit.getImage("images/bk.gif");
        nIcon = resourceKit.getImage("images/bn.gif");
        pIcon = resourceKit.getImage("images/bp.gif");
        RIcon = resourceKit.getImage("images/wr.gif");
        BIcon = resourceKit.getImage("images/wb.gif");
        QIcon = resourceKit.getImage("images/wq.gif");
        KIcon = resourceKit.getImage("images/wk.gif");
        NIcon = resourceKit.getImage("images/wn.gif");
        PIcon = resourceKit.getImage("images/wp.gif");
        this.addMouseListener(new MousePressHandler());
        this.addMouseMotionListener(new MouseMovementHandler());
    }


    public void newGame(int gameMode) {
        game = new Game(gameMode);
        this.gameMode = gameMode;
        winner = null;
        Game_Started = true;
        Is_Local = true;
        repaint();
    }


    private class MousePressHandler extends MouseAdapter {

        public void mousePressed(MouseEvent event) {
            if (winner != null)
                return;
            int clickedColumn = (event.getX() - CELL_DIMENSION / 2) / CELL_DIMENSION;
            int clickedRow = (event.getY() - CELL_DIMENSION / 2) / CELL_DIMENSION;
            Board board = game.getGameState();
            if (selectedPiece == null && board.getPiece(clickedRow, clickedColumn) != null) {
                selectedRow = clickedRow;
                selectedCol = clickedColumn;
                selectedPiece = board.getPiece(selectedRow, selectedCol);
                legalMoves = selectedPiece.generateLegalMoves(board, new Position(selectedRow, selectedCol));
            } else {
                if (selectedPiece != null) {
                    if ((Is_Server && (selectedPiece.getSide() == Piece.Side.black)) || (Is_Client && (selectedPiece.getSide() == Piece.Side.white))) {
                        System.out.println("Can't move");
                        JOptionPane.showMessageDialog(parentWindow,
                                "It's not your turn to move!!",
                                "Wrong turn",
                                JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        Move move = new Move(new Position(selectedRow, selectedCol), new Position(clickedRow, clickedColumn), null);
                        int canMakeMove = game.canMakeMove(move);
                        switch (canMakeMove) {
                            case Game.MOVE_SUCCESS:
                                Player playerMoved = game.getPlayer(board.getPiece(move.source).getSide());
                                Player otherPlayer = game.getPlayer(board.getPiece(move.source).getSide() == Piece.Side.white
                                        ? Piece.Side.black : Piece.Side.white);
                                playerMoved.endTimedMove();
                                playerMoved.incrementMovesMade();
                                Piece pi = game.getPiece(new Position(selectedRow, selectedCol));
                                game.makeMove(move);
                                if (gameMode == Game.GAME_SERVER || gameMode == Game.GAME_CLIENT) {

                                    Mess = pi.getCode() + Integer.toString(clickedRow) + Integer.toString(clickedColumn);
                                    Send_Mess();
                                    System.out.println(Mess);
                                }
                                parentWindow.updateSidePanels();
                                paintImmediately(0, 0, 630, 630);
                                boolean hasGameJustEnded = checkForVictory();
                                if (gameMode == Game.GAMEMODE_SINGLEPLAYER && !hasGameJustEnded) {
                                    otherPlayer.startTimedMove();
                                    parentWindow.updateSidePanels();
                                    Move cpuMove = game.getComputerPlayer().getNextMove(board);
                                    otherPlayer.endTimedMove();
                                    otherPlayer.incrementMovesMade();
                                    game.makeMove(cpuMove);
                                    parentWindow.repaint();
                                    playerMoved.startTimedMove();
                                    checkForVictory();
                                } else {
                                    otherPlayer.startTimedMove();
                                }
                                break;
                            case Game.MOVE_CHECKMATE:
                                JOptionPane.showMessageDialog(parentWindow,
                                        "This move will result in a checkmate!",
                                        "Move into check!",
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                            case Game.MOVE_WRONG_TURN:
                                JOptionPane.showMessageDialog(parentWindow,
                                        "It's not your turn to move!",
                                        "Wrong turn",
                                        JOptionPane.INFORMATION_MESSAGE);
                                break;
                            case Game.MOVE_ERROR:
                                break;
                            default:
                                break;
                        }
                    }
                    parentWindow.updateSidePanels();
                    selectedPiece = null;
                }
            }
            repaint();
        }

    }


    private class MouseMovementHandler extends MouseMotionAdapter {
        public void mouseMoved(MouseEvent event) {
            Board board = game.getGameState();
            Point mousePosition = event.getPoint();
            highlightedCol = (mousePosition.x - CELL_DIMENSION / 2) / CELL_DIMENSION;
            highlightedRow = (mousePosition.y - CELL_DIMENSION / 2) / CELL_DIMENSION;
            highlightedPiece = board.getPiece(highlightedRow, highlightedCol);
            repaint();
        }
    }


    public void paintComponent(Graphics context) {
        super.paintComponent(context);
        Board board = game.getGameState();
        Graphics2D painter = (Graphics2D) context;
        painter.drawImage(background, 0, 0, this);
        // Mark the currently selected piece
        if (selectedPiece != null) {
            painter.drawImage(selectedHighlight, CELL_DIMENSION / 2 + selectedCol * CELL_DIMENSION,
                    CELL_DIMENSION / 2 + selectedRow * CELL_DIMENSION, this);
            for (Move move : legalMoves) {
                painter.drawImage(pathHighlight, CELL_DIMENSION / 2 + move.target.col * CELL_DIMENSION,
                        CELL_DIMENSION / 2 + move.target.row * CELL_DIMENSION, this);
            }
        }
        // Paint the hover effect when a piece is highlighted
        if (highlightedPiece != null && (selectedRow != highlightedRow || selectedCol != highlightedCol)) {
            painter.drawImage(hoverHighlight, CELL_DIMENSION / 2 + highlightedCol * CELL_DIMENSION,
                    CELL_DIMENSION / 2 + highlightedRow * CELL_DIMENSION, this);
        }
        // Paint pieces on the board
        for (int row = 0; row < 8; row++) {
            for (int column = 0; column < 8; column++) {
                Piece piece = board.getPiece(row, column);
                int currentX = CELL_DIMENSION / 2 + (column) * CELL_DIMENSION + CELL_PADDING;
                int currentY = CELL_DIMENSION / 2 + (row) * CELL_DIMENSION;
                if (piece != null) {
                    String type = piece.typeOfPiece();
                    Piece.Side side = piece.getSide();
                    if (type.equals("Pawn")) {
                        if (side.equals(Piece.Side.black))
                            painter.drawImage(pIcon, currentX, currentY, this);
                        else
                            painter.drawImage(PIcon, currentX, currentY, this);
                    } else if (type.equals("Rook")) {
                        if (side.equals(Piece.Side.black))
                            painter.drawImage(rIcon, currentX, currentY, this);
                        else
                            painter.drawImage(RIcon, currentX, currentY, this);
                    } else if (type.equals("Knight")) {

                        if (side.equals(Piece.Side.black))
                            painter.drawImage(nIcon, currentX, currentY, this);
                        else
                            painter.drawImage(NIcon, currentX, currentY, this);
                    } else if (type.equals("Bishop")) {

                        if (side.equals(Piece.Side.black))
                            painter.drawImage(bIcon, currentX, currentY, this);
                        else
                            painter.drawImage(BIcon, currentX, currentY, this);
                    } else if (type.equals("Queen")) {
                        if (side.equals(Piece.Side.black))
                            painter.drawImage(qIcon, currentX, currentY, this);
                        else
                            painter.drawImage(QIcon, currentX, currentY, this);
                    } else if (type.equals("King")) {
                        if (side.equals(Piece.Side.black))
                            painter.drawImage(kIcon, currentX, currentY, this);
                        else
                            painter.drawImage(KIcon, currentX, currentY, this);
                    }
                }
            }
        }
    }


    private boolean checkForVictory() {
        winner = game.getWinner();
        if (winner == null) {
            return false;
        }
        draw = game.getStateGameDraw();
        if (draw == Game.DRAW_STALEMATE) {
            JOptionPane.showMessageDialog(parentWindow,
                    "Stalemate!",
                    "Game Over", JOptionPane.INFORMATION_MESSAGE);
        } else if (draw == Game.DRAW_FIFTYMOVE) {
            JOptionPane.showMessageDialog(parentWindow,
                    "Draw game: No captures have been made and no pawns have been moved in the last fifty moves!",
                    "Game Over", JOptionPane.INFORMATION_MESSAGE);

        } else if (draw == Game.DRAW_THREEFOLD) {
            JOptionPane.showMessageDialog(parentWindow,
                    "Draw game: The same piece positioning has occurred three times consecutively.",
                    "Game Over", JOptionPane.INFORMATION_MESSAGE);
        } else if (draw == Game.DRAW_IMPOSSIBLE) {
            JOptionPane.showMessageDialog(parentWindow,
                    "Draw game: There are not enough pieces on the board for either side to win!",
                    "Game Over", JOptionPane.INFORMATION_MESSAGE);
        } else {
            String side = winner == Piece.Side.white ? "White" : "Black";
            JOptionPane.showMessageDialog(parentWindow,
                    "Checkmate by " + side + "!",
                    "Game Over", JOptionPane.INFORMATION_MESSAGE);
        }
        return true;
    }

    public Player getBlackPlayer() {
        return game.getPlayer(Piece.Side.black);
    }

    public Player getWhitePlayer() {
        return game.getPlayer(Piece.Side.white);
    }

    public Game getGame() {
        return game;
    }
}


