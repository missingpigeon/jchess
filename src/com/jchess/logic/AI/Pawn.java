package com.jchess.logic.AI;

import java.util.ArrayList;

import com.jchess.logic.shared.Move;
import com.jchess.logic.shared.Position;

import static com.jchess.logic.AI.Piece.Side.white;


public class Pawn extends Piece {

    public Pawn(Side side) {
        super(side);
    }


    public ArrayList<Move> generateLegalMoves(Board board, Position pos) {

        ArrayList<Move> legalMoves = new ArrayList<Move>();

        if (side == white) {

            Piece left = board.getPiece(pos.row, pos.col - 1);

            if (left != null && left.getClass() == this.getClass() &&
                    ((Pawn) left).canBePassed(board) && left.side != side) {

                legalMoves.add(new Move(pos, new Position(pos.row - 1, pos.col - 1),
                        new Move(new Position(pos.row, pos.col - 1), null, null), board));
            }

            Piece right = board.getPiece(pos.row, pos.col + 1);

            if (right != null && right.getClass() == this.getClass() &&
                    ((Pawn) right).canBePassed(board) && right.side != side) {

                legalMoves.add(new Move(pos, new Position(pos.row - 1, pos.col + 1),
                        new Move(new Position(pos.row, pos.col + 1), null, null), board));
            }

            if (board.getPiece(pos.row - 1, pos.col + 1) != null) {

                legalMoves.add(new Move(pos, new Position(pos.row - 1, pos.col + 1), board));
            }

            if (board.getPiece(pos.row - 1, pos.col - 1) != null) {

                legalMoves.add(new Move(pos, new Position(pos.row - 1, pos.col - 1), board));
            }

            if (board.getPiece(pos.row - 1, pos.col) == null) {

                legalMoves.add(new Move(pos, new Position(pos.row - 1, pos.col), board));

                if (board.getPiece(pos.row - 2, pos.col) == null && pos.row == board.length() - 2) {

                    legalMoves.add(new Move(pos, new Position(pos.row - 2, pos.col), board));
                }
            }
        } else {

            Piece left = board.getPiece(pos.row, pos.col - 1);

            if (left != null && left.getClass() == this.getClass() &&
                    ((Pawn) left).canBePassed(board) && left.side != side) {

                legalMoves.add(new Move(pos, new Position(pos.row + 1, pos.col - 1),
                        new Move(new Position(pos.row, pos.col - 1), null, null), board));
            }

            Piece right = board.getPiece(pos.row, pos.col + 1);

            if (right != null && right.typeOfPiece().equals("Pawn") &&
                    ((Pawn) right).canBePassed(board) && right.side != side) {

                legalMoves.add(new Move(pos, new Position(pos.row + 1, pos.col + 1),
                        new Move(new Position(pos.row, pos.col + 1), null, null), board));
            }

            if (board.getPiece(pos.row + 1, pos.col + 1) != null) {

                legalMoves.add(new Move(pos, new Position(pos.row + 1, pos.col + 1), board));
            }

            if (board.getPiece(pos.row + 1, pos.col - 1) != null) {

                legalMoves.add(new Move(pos, new Position(pos.row + 1, pos.col - 1), board));
            }

            if (board.getPiece(pos.row + 1, pos.col) == null) {

                legalMoves.add(new Move(pos, new Position(pos.row + 1, pos.col), board));

                if (board.getPiece(pos.row + 2, pos.col) == null && pos.row == 1) {

                    legalMoves.add(new Move(pos, new Position(pos.row + 2, pos.col), board));
                }
            }
        }

        for (int i = 0; i < legalMoves.size(); i++) {

            Move move = legalMoves.get(i);
            Piece piece = board.getPiece(move.target);

            if (piece != null && piece.getSide() == board.getPiece(move.source).getSide()) {

                legalMoves.remove(i);

                i--;
            }
        }

        return legalMoves;
    }


    public String typeOfPiece() {
        return "Pawn";
    }


    public int getPieceValue() {
        return 100;
    }


    public boolean canBePassed(Board board) {

        Integer timesMoved = board.piecesMovedCount.get(this);

        if (timesMoved != null && timesMoved == 1) {
            return true;
        }
        return false;
    }


    public int getPieceID() {
        return Board.PAWN;
    }


    public String getCode() {
        return code;
    }
}
