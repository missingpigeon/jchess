package com.jchess.logic.AI;

import java.util.ArrayList;

import com.jchess.logic.shared.Move;
import com.jchess.logic.shared.Position;


public class King extends Piece {

    public King(Side side) {
        super(side);
    }


    public ArrayList<Move> generateLegalMoves(Board board, Position pos) {

        ArrayList<Move> legalMoves = new ArrayList<Move>();

        int canCastle = canCastle(board, pos);

        if (canCastle != 0) {
            if (canCastle == 1) {
                legalMoves.add(new Move(pos, new Position(pos.row, 2),
                        new Move(new Position(pos.row, 0),
                                new Position(pos.row, 3), null), board));
            } else {
                legalMoves.add(new Move(pos, new Position(pos.row, 6),
                        new Move(new Position(pos.row, 7),
                                new Position(pos.row, 5), null), board));
            }
        }

        for (int addRow = -1; addRow <= 1; addRow++)
            for (int addCol = -1; addCol <= 1; addCol++) {

                int moveToRow = pos.row + addRow;
                int moveToCol = pos.col + addCol;

                if (!isOnBoard(board, moveToRow, moveToCol))
                    continue;

                Position newPos = new Position(moveToRow, moveToCol);

                Move current = new Move(pos, newPos, board);

                Board newBoard = new Board(board);

                newBoard.makeMove(current);

                if (!isInCheck(newBoard, newPos)) {
                    legalMoves.add(current);
                }
            }

        for (int i = 0; i < legalMoves.size(); i++) {
            Move move = legalMoves.get(i);
            Piece piece = board.getPiece(move.target);

            if (piece != null && piece.getSide() == board.getPiece(move.source).getSide()) {

                legalMoves.remove(i);

                i--;
            }
        }
        return legalMoves;
    }


    public boolean isInCheck(Board board, Position pos) {

        for (int row = 0; row < board.length(); row++) {
            for (int col = 0; col < board.width(); col++) {

                Position currentPos = new Position(row, col);

                Piece current = board.getPiece(currentPos);

                if (current != null && current.getSide() != this.side) {

                    ArrayList<Move> moves = null;

                    if (!(current instanceof King))
                        moves = current.generateLegalMoves(board, currentPos);

                    if (moves != null) {

                        if (current instanceof Pawn) {

                            for (Move move : moves) {

                                if (move.target.equals(pos) && (move.source.col - move.target.col == 1
                                        || move.source.col - move.target.col == -1)) {

                                    return true;
                                }
                            }
                        } else {

                            for (Move move : moves) {

                                if (move.target.equals(pos)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            for (int r = -1; r <= 1; r++) {
                for (int c = -1; c <= 1; c++) {

                    Piece p = board.getPiece(pos.row + r, pos.col + c);

                    if (p instanceof King && p.side != this.side)
                        return true;
                }
            }
        }
        return false;
    }


    public int canCastle(Board board, Position pos) {

        Piece pieceToCheck;

        if (!board.piecesMovedCount.containsKey(this) && !isInCheck(board, pos)) {

            for (int column = 0; column <= 7; column += 7) {

                pieceToCheck = board.getPiece(pos.row, column);

                if (pieceToCheck != null && pieceToCheck.typeOfPiece().equals("Rook")) {

                    boolean obstruction = false;

                    if (column == 0) {

                        for (int addCol = -3; addCol < 0; addCol++) {

                            if (board.getPiece(pos.row, pos.col + addCol) != null || isInCheck(board, new Position(pos.row, pos.col + addCol)))

                                obstruction = true;
                        }
                        if (obstruction == false && ((Rook) pieceToCheck).hasNotMoved(board))

                            return 1;
                    } else {

                        for (int addCol = 1; addCol <= 2; addCol++)

                            if (board.getPiece(pos.row, pos.col + addCol) != null || isInCheck(board, new Position(pos.row, pos.col + addCol)))

                                obstruction = true;

                        if (obstruction == false && ((Rook) pieceToCheck).hasNotMoved(board))

                            return 2;
                    }
                }
            }
        }

        return 0;
    }


    public String typeOfPiece() {
        return "King";
    }


    public int getPieceValue() {
        return 400000;
    }


    public int getPieceID() {
        return Board.KING;
    }


    public String getCode() {
        return code;
    }
}
