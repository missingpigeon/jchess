package com.jchess.logic.AI;

import java.util.ArrayList;

import com.jchess.logic.shared.Move;
import com.jchess.logic.shared.Position;

public class Bishop extends Piece {

    public Bishop(Side side) {
        super(side);
    }


    public ArrayList<Move> generateLegalMoves(Board board, Position pos) {

        ArrayList<Move> legalMoves = new ArrayList<Move>();

        int moveToRow = pos.row;
        int moveToCol = pos.col;

        for (int addRow = -1; addRow <= 1; addRow += 2)
            for (int addCol = -1; addCol <= 1; addCol += 2) {
                while (isOnBoard(board, moveToRow + addRow, moveToCol + addCol)
                        && board.getPiece(moveToRow + addRow, moveToCol + addCol) == null) {

                    moveToRow += addRow;
                    moveToCol += addCol;

                    legalMoves.add(new Move(pos, new Position(moveToRow, moveToCol), board));
                }

                if (isOnBoard(board, moveToRow + addRow, moveToCol + addCol)) {

                    legalMoves.add(new Move(pos, new Position(moveToRow + addRow, moveToCol + addCol), board));
                }

                moveToRow = pos.row;
                moveToCol = pos.col;
            }

        for (int i = 0; i < legalMoves.size(); i++) {

            Move move = legalMoves.get(i);
            Piece piece = board.getPiece(move.target);

            if (piece != null && piece.getSide() == board.getPiece(move.source).getSide()) {

                legalMoves.remove(i);

                i--;
            }
        }
        return legalMoves;
    }


    public String typeOfPiece() {
        return "Bishop";
    }


    public int getPieceValue() {
        return 325;
    }


    public int getPieceID() {
        return Board.BISHOP;
    }


    public String getCode() {
        return code;
    }
}
