package com.jchess.logic.AI;

import java.util.ArrayList;

import com.jchess.logic.shared.Move;
import com.jchess.logic.shared.Position;


public class Knight extends Piece {

    public Knight(Side side) {
        super(side);
    }


    public ArrayList<Move> generateLegalMoves(Board board, Position pos) {

        ArrayList<Move> legalMoves = new ArrayList<Move>();

        for (int addRow = 1; addRow <= 2; addRow++)
            for (int addCol = 1; addCol <= 2; addCol++)

                if (addRow != addCol) {

                    if (isOnBoard(board, pos.row + addRow, pos.col + addCol)) {

                        legalMoves.add(new Move(pos, new Position(pos.row + addRow, pos.col + addCol), board));
                    }

                    if (isOnBoard(board, pos.row + addRow, pos.col - addCol)) {

                        legalMoves.add(new Move(pos, new Position(pos.row + addRow, pos.col - addCol), board));
                    }

                    if (isOnBoard(board, pos.row - addRow, pos.col + addCol)) {

                        legalMoves.add(new Move(pos, new Position(pos.row - addRow, pos.col + addCol), board));
                    }

                    if (isOnBoard(board, pos.row - addRow, pos.col - addCol)) {

                        legalMoves.add(new Move(pos, new Position(pos.row - addRow, pos.col - addCol), board));
                    }
                }

        for (int i = 0; i < legalMoves.size(); i++) {

            Move move = legalMoves.get(i);
            Piece piece = board.getPiece(move.target);

            if (piece != null && piece.getSide() == board.getPiece(move.source).getSide()) {

                legalMoves.remove(i);

                i--;
            }
        }

        return legalMoves;
    }


    public String typeOfPiece() {
        return "Knight";
    }


    public int getPieceValue() {
        return 320;
    }


    public int getPieceID() {
        return Board.KNIGHT;
    }


    public String getCode() {
        return code;
    }
}
