package com.jchess.logic.AI;

import java.util.ArrayList;

import com.jchess.logic.shared.Move;
import com.jchess.logic.shared.Position;


public abstract class Piece {
    public enum Side {white, black}

    ;

    protected Side side;

    protected String code;

    public Piece(Side side) {
        this.side = side;
    }


    public abstract ArrayList<Move> generateLegalMoves(Board board, Position pos);


    public abstract int getPieceValue();


    protected boolean isOnBoard(Board board, int r, int c) {

        return r >= 0 && r < board.length() && c >= 0 && c < board.width();
    }


    public abstract String typeOfPiece();


    public abstract int getPieceID();


    public Side getSide() {
        return side;
    }


    public boolean equals(Object other) {

        if (other instanceof Piece && this.hashCode() == other.hashCode()) {
            return true;
        }
        return false;
    }


    public abstract String getCode();
}
