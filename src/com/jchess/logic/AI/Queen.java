package com.jchess.logic.AI;

import java.util.ArrayList;

import com.jchess.logic.shared.Move;
import com.jchess.logic.shared.Position;


public class Queen extends Piece {

    public Queen(Side side) {
        super(side);
    }


    public ArrayList<Move> generateLegalMoves(Board board, Position pos) {

        ArrayList<Move> legalMoves = new ArrayList<Move>();

        int moveToRow = pos.row;
        int moveToCol = pos.col;

        for (int addRow = -1; addRow <= 1; addRow++)
            for (int addCol = -1; addCol <= 1; addCol++)

                if (addRow != 0 || addCol != 0) {

                    while (isOnBoard(board, moveToRow + addRow, moveToCol + addCol)
                            && board.getPiece(moveToRow + addRow, moveToCol + addCol) == null) {

                        moveToRow += addRow;
                        moveToCol += addCol;

                        legalMoves.add(new Move(pos, new Position(moveToRow, moveToCol), board));
                    }

                    if (isOnBoard(board, moveToRow + addRow, moveToCol + addCol)) {

                        legalMoves.add(new Move(pos, new Position(moveToRow + addRow, moveToCol + addCol), board));
                    }

                    moveToRow = pos.row;
                    moveToCol = pos.col;
                }

        for (int i = 0; i < legalMoves.size(); i++) {

            Move move = legalMoves.get(i);
            Piece piece = board.getPiece(move.target);

            if (piece != null && piece.getSide() == board.getPiece(move.source).getSide()) {

                legalMoves.remove(i);

                i--;
            }
        }

        return legalMoves;
    }


    public String typeOfPiece() {
        return "Queen";
    }


    public int getPieceValue() {
        return 975;
    }


    public int getPieceID() {
        return Board.QUEEN;
    }


    public String getCode() {
        return code;
    }
}
