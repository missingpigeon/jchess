package com.jchess.logic.AI;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.*;

import com.jchess.logic.shared.Move;
import com.jchess.logic.shared.Player;
import com.jchess.logic.shared.Position;

public class Computer extends Player {

    protected int maxDepth;

    protected int captureSearchDepth;

    protected int cpuCount;

    protected int[] moveScores;

    ThreadPoolExecutor threadPool;


    public Computer(Piece.Side side, int depth) {
        super(side);
        maxDepth = depth;
        captureSearchDepth = 1;
        cpuCount = Runtime.getRuntime().availableProcessors();

        threadPool = new ThreadPoolExecutor(cpuCount, cpuCount + 1, 10, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(cpuCount + 1));
        isHuman = false;
    }


    protected class CpuThread implements Runnable {

        Board b;
        List<Move> moves;
        int scoreIndex;

        public CpuThread(Board b, List<Move> moves, int startIndex) {
            this.b = b;
            this.moves = moves;
            this.scoreIndex = startIndex;
        }


        public void run() {

            Piece.Side opponent = (side == Piece.Side.black ? Piece.Side.white : Piece.Side.black);

            for (Move currentMove : moves) {
                Board nextBoard = new Board(b);
                nextBoard.makeMove(currentMove);

                int score = -findMoveScore(nextBoard, maxDepth, opponent, Integer.MIN_VALUE, Integer.MAX_VALUE);

                moveScores[scoreIndex++] = score;
            }
        }
    }


    public Move getNextMove(Board b) {
        long startTime = System.currentTimeMillis();

        ArrayList<Move> legalMoves = new ArrayList<Move>(b.getAllPossibleMovesBySide(side));

        for (int i = 0; i < legalMoves.size(); i++) {
            Move current = legalMoves.get(i);
            Board test = new Board(b);
            test.makeMove(current);
            King king;
            Position pos;
            if (side == Piece.Side.white)
                pos = test.getWhiteKingPos();
            else
                pos = test.getBlackKingPos();
            king = (King) test.getPiece(pos);
            if (king.isInCheck(test, pos)) {
                legalMoves.remove(i);
                i--;
            }
        }

        moveScores = new int[legalMoves.size()];

        int jobsPerThread = legalMoves.size() / cpuCount;

        Collection<Future<?>> futures = new LinkedList<Future<?>>();

        for (int thread = 0; thread < cpuCount; thread++) {

            CpuThread jobs;
            if (thread == cpuCount - 1) {
                jobs = new CpuThread(b, legalMoves.subList(thread * jobsPerThread, legalMoves.size()), thread * jobsPerThread);
            } else {
                jobs = new CpuThread(b, legalMoves.subList(thread * jobsPerThread, thread * jobsPerThread + jobsPerThread), thread * jobsPerThread);
            }
            futures.add(threadPool.submit(jobs));
        }

        try {
            for (Future<?> future : futures)
                future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        int bestMoveIndex = 0;
        for (int i = 1; i < moveScores.length; i++) {
            if (moveScores[i] > moveScores[bestMoveIndex])
                bestMoveIndex = i;
        }

        System.out.println("AI took: " + (System.currentTimeMillis() - startTime) + "ms");

        return legalMoves.get(bestMoveIndex);
    }


    public int findMoveScore(Board b, int depth, Piece.Side s, int lowerLimit, int upperLimit) {

        if (depth == 1)

            return findMoveScoreCaptureOnly(b, captureSearchDepth, s, lowerLimit, upperLimit);

        int bestScore = Integer.MIN_VALUE;

        Piece.Side opponent = (s == Piece.Side.black ? Piece.Side.white : Piece.Side.black);

        PriorityQueue<Move> legalMoves = b.getAllPossibleMovesBySide(s);

        while (!legalMoves.isEmpty()) {

            Move currentMove = legalMoves.remove();
            Board nextBoard = new Board(b);
            nextBoard.makeMove(currentMove);

            int currentScore = -findMoveScore(nextBoard, depth - 1, opponent, -upperLimit, -lowerLimit);

            if (currentScore > bestScore)
                bestScore = currentScore;

            if (currentScore > lowerLimit)
                lowerLimit = currentScore;

            if (lowerLimit >= upperLimit)
                return lowerLimit;
        }

        return bestScore;
    }


    public int findMoveScoreCaptureOnly(Board b, int depth, Piece.Side s, int lowerLimit, int upperLimit) {

        if (depth == 1)
            return b.getScore(side) * (s == side ? 1 : -1);

        int bestScore = Integer.MIN_VALUE;

        Piece.Side opponent = (s == Piece.Side.black ? Piece.Side.white : Piece.Side.black);

        PriorityQueue<Move> captureMoves = b.getAllCaptureMovesBySide(s);
        if (captureMoves.size() == 0)
            return b.getScore(side) * (s == side ? 1 : -1);

        while (!captureMoves.isEmpty()) {
            Move currentMove = captureMoves.remove();
            Board nextBoard = new Board(b);
            nextBoard.makeMove(currentMove);

            int currentScore = -findMoveScoreCaptureOnly(nextBoard, depth - 1, opponent, -upperLimit, -lowerLimit);

            if (currentScore > bestScore)
                bestScore = currentScore;

            if (currentScore > lowerLimit)
                lowerLimit = currentScore;

            if (lowerLimit >= upperLimit)
                return lowerLimit;
        }

        return bestScore;
    }
}
