package com.jchess.logic.AI;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

import com.jchess.logic.shared.Game;
import com.jchess.logic.shared.Move;
import com.jchess.logic.shared.Position;

import static com.jchess.logic.AI.Piece.Side.black;
import static com.jchess.logic.AI.Piece.Side.white;

public class Board {
    public static final int pieceScores[][][][] = {
            {
                    // Bishop
                    {
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {7, 3, 3, 3, 3, 3, 3, 7},
                            {7, 6, 3, 6, 3, 6, 7, 7},
                            {7, 3, 7, 3, 3, 3, 3, 7},
                            {1, 3, 3, 3, 3, 3, 3, 1},
                            {1, 1, 1, 1, 1, 1, 1, 1},
                            {3, 3, 3, 3, 3, 3, 3, 3}
                    },
                    // King
                    {
                            {0, 0, 0, 0, 0, 0, 0, 0},
                            {10, 10, 10, 10, 10, 10, 10, 10},
                            {0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 10, 10, 10, 10, 10, 10, 0},
                            {0, 10, 10, 10, 10, 10, 10, 0},
                            {0, 10, 10, 10, 10, 10, 10, 0},
                            {10, 10, 10, 10, 10, 10, 10, 10},
                            {60, 60, 60, 25, 30, 25, 60, 60}
                    },
                    // Knight
                    {
                            {7, 7, 7, 7, 7, 7, 7, 7},
                            {1, 1, 1, 1, 1, 1, 1, 1},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 4, 3, 4, 4, 3, 4, 4},
                            {3, 4, 3, 4, 3, 4, 4, 3},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {1, 1, 1, 1, 1, 1, 1, 1},
                            {0, 0, 0, 0, 0, 0, 0, 0}
                    },
                    // Pawn
                    {
                            {300, 300, 300, 300, 300, 300, 300, 300},
                            {40, 40, 40, 40, 40, 40, 40, 40},
                            {20, 10, 20, 10, 20, 10, 20, 10},
                            {5, 4, 5, 4, 5, 4, 5, 4},
                            {4, 4, 3, 4, 4, 4, 4, 4},
                            {2, 2, 4, 2, 5, 2, 4, 2},
                            {1, 2, 3, 3, 2, 3, 3, 1},
                            {0, 0, 0, 0, 0, 0, 0, 0}
                    },
                    // Queen
                    {
                            {6, 6, 6, 6, 6, 6, 6, 6},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 3, 3, 3, 3, 3, 3, 8},
                            {6, 6, 6, 6, 6, 6, 6, 6},
                            {6, 6, 6, 6, 6, 6, 6, 6},
                            {6, 6, 6, 6, 6, 6, 6, 6},
                            {0, 0, 0, 3, 3, 0, 0, 0}
                    },
                    // Rook
                    {
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 1, 1, 1, 1, 1, 1, 3},
                            {1, 3, 3, 3, 3, 3, 3, 1},
                            {1, 3, 3, 3, 3, 3, 3, 1},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {1, 3, 3, 3, 3, 3, 3, 1},
                            {1, 0, 4, 4, 4, 4, 0, 1}
                    }
            },
            // Mid
            {
                    // Bishop
                    {
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {7, 3, 3, 3, 3, 3, 3, 7},
                            {7, 6, 3, 6, 3, 6, 7, 7},
                            {7, 3, 7, 3, 3, 3, 3, 7},
                            {1, 3, 3, 3, 3, 3, 3, 1},
                            {1, 1, 1, 1, 1, 1, 1, 1},
                            {3, 3, 3, 3, 3, 3, 3, 3}
                    },
                    // King
                    {
                            {0, 0, 0, 0, 0, 0, 0, 0},
                            {10, 10, 10, 10, 10, 10, 10, 10},
                            {0, 0, 0, 0, 0, 0, 0, 0},
                            {0, 10, 10, 10, 10, 10, 10, 0},
                            {0, 10, 10, 10, 10, 10, 10, 0},
                            {0, 10, 10, 10, 10, 10, 10, 0},
                            {10, 10, 10, 10, 10, 10, 10, 10},
                            {60, 60, 60, 25, 30, 25, 60, 60}
                    },
                    // Knight
                    {
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {1, 1, 1, 1, 1, 1, 1, 1},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 4, 3, 4, 4, 3, 4, 4},
                            {3, 4, 3, 4, 3, 4, 4, 3},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {1, 1, 1, 1, 1, 1, 1, 1},
                            {0, 0, 0, 0, 0, 0, 0, 0}
                    },
                    // Pawn
                    {
                            {300, 300, 300, 300, 300, 300, 300, 300},
                            {110, 110, 110, 110, 110, 110, 110, 110},
                            {20, 10, 20, 10, 20, 10, 20, 10},
                            {10, 20, 10, 20, 10, 20, 10, 20},
                            {4, 4, 3, 4, 4, 4, 4, 4},
                            {2, 2, 4, 2, 5, 2, 4, 2},
                            {1, 2, 3, 3, 2, 3, 3, 1},
                            {0, 0, 0, 0, 0, 0, 0, 0}
                    },
                    // Queen
                    {
                            {6, 6, 6, 6, 6, 6, 6, 6},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 3, 3, 3, 3, 3, 3, 8},
                            {6, 6, 6, 6, 6, 6, 6, 6},
                            {6, 6, 6, 6, 6, 6, 6, 6},
                            {6, 6, 6, 6, 6, 6, 6, 6},
                            {0, 0, 0, 3, 3, 0, 0, 0}
                    },
                    // Rook
                    {
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 1, 1, 1, 1, 1, 1, 3},
                            {1, 3, 3, 3, 3, 3, 3, 1},
                            {1, 3, 3, 3, 3, 3, 3, 1},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {1, 3, 3, 3, 3, 3, 3, 1},
                            {1, 0, 4, 4, 4, 4, 0, 1}
                    }
            },
            // Late
            {
                    // Bishop
                    {
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {7, 3, 3, 3, 3, 3, 3, 7},
                            {7, 6, 3, 6, 3, 6, 7, 7},
                            {7, 3, 7, 3, 3, 3, 3, 7},
                            {1, 3, 3, 3, 3, 3, 3, 1},
                            {1, 1, 1, 1, 1, 1, 1, 1},
                            {3, 3, 3, 3, 3, 3, 3, 3}
                    },
                    // King
                    {
                            {0, 5, 5, 5, 5, 5, 5, 0},
                            {10, 10, 10, 10, 10, 10, 10, 10},
                            {10, 10, 10, 10, 10, 10, 10, 10},
                            {0, 12, 14, 14, 14, 14, 12, 0},
                            {0, 12, 14, 14, 14, 14, 12, 0},
                            {0, 12, 14, 14, 14, 14, 12, 0},
                            {10, 10, 10, 10, 10, 10, 10, 10},
                            {0, 5, 5, 5, 5, 5, 5, 0}
                    },
                    // Knight
                    {
                            {2, 2, 2, 2, 2, 2, 2, 2},
                            {1, 1, 1, 1, 1, 1, 1, 1},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 4, 3, 4, 4, 3, 4, 4},
                            {3, 4, 3, 4, 3, 4, 4, 3},
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {1, 1, 1, 1, 1, 1, 1, 1},
                            {2, 2, 2, 2, 2, 2, 2, 2}
                    },
                    // Pawn
                    {
                            {400, 400, 400, 400, 400, 400, 400, 400},
                            {300, 300, 300, 300, 300, 300, 300, 300},
                            {160, 160, 160, 160, 160, 160, 160, 160},
                            {80, 80, 80, 80, 80, 80, 80, 80},
                            {40, 40, 40, 40, 40, 40, 40, 40},
                            {20, 20, 20, 20, 20, 20, 20, 20},
                            {10, 10, 10, 10, 10, 10, 10, 10},
                            {0, 0, 0, 0, 0, 0, 0, 0}
                    },
                    // Queen
                    {
                            {6, 6, 6, 6, 6, 6, 6, 6},
                            {6, 20, 20, 20, 20, 20, 20, 6},
                            {6, 20, 25, 25, 25, 25, 20, 6},
                            {6, 20, 25, 30, 30, 25, 20, 6},
                            {6, 20, 25, 30, 30, 25, 20, 6},
                            {6, 20, 25, 25, 25, 25, 20, 6},
                            {6, 20, 20, 20, 20, 20, 20, 6},
                            {6, 6, 6, 6, 6, 6, 6, 6}
                    },
                    // Rook
                    {
                            {3, 3, 3, 3, 3, 3, 3, 3},
                            {3, 1, 1, 1, 1, 1, 1, 3},
                            {1, 4, 4, 3, 3, 4, 4, 1},
                            {1, 4, 4, 3, 3, 4, 4, 1},
                            {3, 4, 4, 3, 3, 4, 4, 3},
                            {3, 4, 4, 3, 3, 4, 4, 3},
                            {1, 3, 3, 3, 3, 3, 3, 1},
                            {1, 3, 3, 3, 3, 3, 3, 1}
                    }
            }
    };

    public static final int BISHOP = 0;
    public static final int KING = 1;
    public static final int KNIGHT = 2;
    public static final int PAWN = 3;
    public static final int QUEEN = 4;
    public static final int ROOK = 5;

    private Piece[][] board;

    protected HashMap<Piece, Integer> piecesMovedCount;

    protected Position whiteKing;
    protected Position blackKing;
    protected int pieceCount;
    protected int whitePieceCount;
    protected int blackPieceCount;
    protected int passiveMoveCount;

    private int gamestate;


    public Board() {

        board = new Piece[8][8];
        startNewGame();

        piecesMovedCount = new HashMap<Piece, Integer>();
        passiveMoveCount = 0;
    }


    public void startNewGame() {
        // Black
        board[0][0] = new Rook(black);
        board[0][1] = new Knight(black);
        board[0][2] = new Bishop(black);
        board[0][3] = new Queen(black);
        board[0][4] = new King(black);
        board[0][5] = new Bishop(black);
        board[0][6] = new Knight(black);
        board[0][7] = new Rook(black);
        board[1][0] = new Pawn(black);
        board[1][1] = new Pawn(black);
        board[1][2] = new Pawn(black);
        board[1][3] = new Pawn(black);
        board[1][4] = new Pawn(black);
        board[1][5] = new Pawn(black);
        board[1][6] = new Pawn(black);
        board[1][7] = new Pawn(black);
        // Set Black code
        board[0][0].code = "br1";
        board[0][1].code = "bn1";
        board[0][2].code = "bb1";
        board[0][3].code = "bq1";
        board[0][4].code = "bk1";
        board[0][5].code = "bb2";
        board[0][6].code = "bn2";
        board[0][7].code = "br2";
        board[1][0].code = "bp1";
        board[1][1].code = "bp2";
        board[1][2].code = "bp3";
        board[1][3].code = "bp4";
        board[1][4].code = "bp5";
        board[1][5].code = "bp6";
        board[1][6].code = "bp7";
        board[1][7].code = "bp8";
        // White
        board[7][0] = new Rook(white);
        board[7][1] = new Knight(white);
        board[7][2] = new Bishop(white);
        board[7][3] = new Queen(white);
        board[7][4] = new King(white);
        board[7][5] = new Bishop(white);
        board[7][6] = new Knight(white);
        board[7][7] = new Rook(white);
        board[6][0] = new Pawn(white);
        board[6][1] = new Pawn(white);
        board[6][2] = new Pawn(white);
        board[6][3] = new Pawn(white);
        board[6][4] = new Pawn(white);
        board[6][5] = new Pawn(white);
        board[6][6] = new Pawn(white);
        board[6][7] = new Pawn(white);
        // Set White code
        board[7][0].code = "wr1";
        board[7][1].code = "wn1";
        board[7][2].code = "wb1";
        board[7][3].code = "wq1";
        board[7][4].code = "wk1";
        board[7][5].code = "wb2";
        board[7][6].code = "wn2";
        board[7][7].code = "wr2";
        board[6][0].code = "wp1";
        board[6][1].code = "wp2";
        board[6][2].code = "wp3";
        board[6][3].code = "wp4";
        board[6][4].code = "wp5";
        board[6][5].code = "wp6";
        board[6][6].code = "wp7";
        board[6][7].code = "wp8";

        pieceCount = 32;
        whitePieceCount = blackPieceCount = 16;

        blackKing = new Position(0, 4);
        whiteKing = new Position(7, 4);
    }


    public Board(Board b) {

        board = new Piece[8][8];

        for (int r = 0; r < board.length; r++)
            for (int c = 0; c < board[r].length; c++)
                board[r][c] = b.board[r][c];

        piecesMovedCount = new HashMap<Piece, Integer>(b.piecesMovedCount);
        whiteKing = new Position(b.whiteKing);
        blackKing = new Position(b.blackKing);
        this.passiveMoveCount = b.passiveMoveCount;
        this.pieceCount = b.pieceCount;
        this.whitePieceCount = b.whitePieceCount;
        this.blackPieceCount = b.blackPieceCount;
        gamestate = 0;
    }


    public Piece getPiece(Position p) {

        if (p.row >= 0 && p.row < board.length && p.col >= 0 && p.col < board[0].length)
            return board[p.row][p.col];
        return null;
    }


    public Position getPosition(String code) {

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (board[i][j] != null && board[i][j].code.equals(code))
                    return new Position(i, j);
            }
        }
        return null;
    }


    public Piece getPiece(int r, int c) {

        if (r >= 0 && r < board.length && c >= 0 && c < board[0].length)
            return board[r][c];
        return null;
    }


    public int length() {
        return board.length;
    }


    public int width() {
        return board[0].length;
    }


    public PriorityQueue<Move> getAllPossibleMovesBySide(Piece.Side side) {

        List<Move> moves = new ArrayList<Move>();
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[0].length; col++) {
                if (board[row][col] != null && board[row][col].getSide() == side) {

                    moves.addAll(board[row][col].generateLegalMoves(this, new Position(row, col)));
                }
            }
        }
        return new PriorityQueue<Move>(moves);
    }


    public PriorityQueue<Move> getAllCaptureMovesBySide(Piece.Side side) {

        List<Move> moves = new ArrayList<Move>();
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[0].length; col++) {
                if (board[row][col] != null && board[row][col].getSide() == side) {

                    List<Move> legalMoves = board[row][col].generateLegalMoves(this, new Position(row, col));

                    for (int i = 0; i < legalMoves.size(); i++) {
                        Move current = legalMoves.get(i);
                        if (getPiece(current.target) == null) {
                            legalMoves.remove(i);
                            i--;
                        }
                    }

                    moves.addAll(legalMoves);
                }
            }
        }

        return new PriorityQueue<Move>(moves);
    }


    public int checkForDraw() {
        return gamestate;
    }


    public Piece.Side checkForWins(Piece.Side currentPlayerSide) {

        Piece.Side otherPlayerSide = (currentPlayerSide == black ? white : black);
        King king;

        if (otherPlayerSide == white) {
            king = (King) getPiece(whiteKing);
            if (king.isInCheck(this, whiteKing) && isCheckmated(otherPlayerSide, whiteKing))
                return currentPlayerSide;
        } else {
            king = (King) getPiece(blackKing);
            if (king.isInCheck(this, blackKing) && isCheckmated(otherPlayerSide, blackKing))
                return currentPlayerSide;
        }


        PriorityQueue<Move> moves = this.getAllPossibleMovesBySide(otherPlayerSide);
        if (moves.size() == 0)
            gamestate = Game.DRAW_STALEMATE;

        for (Move move : moves) {
            Board testBoard = new Board(this);
            testBoard.makeMove(move);
            Position kingPos;
            if (otherPlayerSide == white)
                kingPos = testBoard.getWhiteKingPos();
            else
                kingPos = testBoard.getBlackKingPos();
            king = (King) testBoard.getPiece(kingPos);

            if (!king.isInCheck(testBoard, kingPos))
                return null;
        }

        gamestate = Game.DRAW_STALEMATE;

        return null;
    }


    private boolean isCheckmated(Piece.Side side, Position king) {

        Collection<Move> moves = this.getAllPossibleMovesBySide(side);
        for (Move move : moves) {
            Board newBoard = new Board(this);
            newBoard.makeMove(move);

            if (!((King) newBoard.getPiece(side == black ? newBoard.blackKing : newBoard.whiteKing)).isInCheck(newBoard, (side == black ? newBoard.blackKing : newBoard.whiteKing)))
                return false;
        }
        return true;
    }


    public int getScore(Piece.Side cpuSide) {

        if (cpuSide == white) {
            if (board[whiteKing.row][whiteKing.col] == null)
                return Integer.MIN_VALUE + 1;
        } else {
            if (board[blackKing.row][blackKing.col] == null)
                return Integer.MIN_VALUE + 1;
        }

        int score = 0;

        for (int r = 0; r < board.length; r++) {
            for (int c = 0; c < board[r].length; c++) {
                if (board[r][c] != null) {

                    if (board[r][c].getSide() == cpuSide) {
                        score += board[r][c].getPieceValue();

                        if (cpuSide == white) {
                            score += pieceScores[getTableVersion()][board[r][c].getPieceID()][r][c];
                        } else {
                            score += pieceScores[getTableVersion()][board[r][c].getPieceID()][7 - r][c];
                        }
                    } else {

                        score -= board[r][c].getPieceValue();

                        if (cpuSide == black) {
                            score -= pieceScores[getTableVersion()][board[r][c].getPieceID()][r][c];
                        } else {
                            score -= pieceScores[getTableVersion()][board[r][c].getPieceID()][7 - r][c];
                        }
                    }
                }
            }
        }

        if (pieceCount <= 16) {

            int distWeight = (16 - pieceCount) * 10;
            int distScore = 0;
            for (int r = 0; r < board.length; r++) {
                for (int c = 0; c < board[r].length; c++) {
                    if (board[r][c] != null) {
                        if (board[r][c].getSide() == cpuSide) {

                            if (cpuSide == white) {
                                distScore -= Math.abs(r - blackKing.row) + Math.abs(c - blackKing.col);
                            } else {
                                distScore -= Math.abs(r - whiteKing.row) + Math.abs(c - whiteKing.col);
                            }
                        } else {

                            if (cpuSide == white) {
                                distScore += Math.abs(r - whiteKing.row) + Math.abs(c - whiteKing.col);
                            } else {
                                distScore += Math.abs(r - blackKing.row) + Math.abs(c - blackKing.col);
                            }
                        }
                    }
                }

            }
            score += distScore * distWeight;
        }

        return score;
    }


    public void makeMove(Move move) {

        Piece src = getPiece(move.source);

        if (!piecesMovedCount.containsKey(src))
            piecesMovedCount.put(src, 1);
        else
            piecesMovedCount.put(src, piecesMovedCount.get(src) + 1);

        if (src instanceof King) {
            if (src.getSide() == white)
                whiteKing = move.target;
            else
                blackKing = move.target;
        } else if (src instanceof Pawn) {

            if (move.target.row == 0 || move.target.row == 7)
                board[move.source.row][move.source.col] = Game.currentGame.getPlayer(src.getSide()).getPromotePiece();
            passiveMoveCount = 0;
        }

        if (board[move.target.row][move.target.col] != null) {
            passiveMoveCount = 0;
            decrementPieceCount(board[move.target.row][move.target.col].getSide());
        } else {
            passiveMoveCount++;
        }

        board[move.target.row][move.target.col] = board[move.source.row][move.source.col];
        board[move.source.row][move.source.col] = null;

        if (move.extra != null) {
            if (move.extra.target != null) {

                board[move.extra.target.row][move.extra.target.col] = board[move.extra.source.row][move.extra.source.col];
            } else {

                decrementPieceCount(board[move.extra.source.row][move.extra.source.col].getSide());
            }
            board[move.extra.source.row][move.extra.source.col] = null;
            passiveMoveCount = 0;
        }
    }


    public Position getWhiteKingPos() {
        return whiteKing;
    }


    public Position getBlackKingPos() {
        return blackKing;
    }


    public HashMap<Piece, Integer> getPiecesMovedCount() {
        return piecesMovedCount;
    }


    public boolean equals(Object otherObject) {
        if (!(otherObject instanceof Board))
            return false;

        Board other = (Board) otherObject;
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (this.board[row][col] != other.board[row][col])
                    return false;
            }
        }
        return true;
    }


    public int getConsecPassiveMovesCount() {
        return passiveMoveCount;
    }


    public int getPiecesCount() {
        return pieceCount;
    }


    public int getPiecesCount(Piece.Side side) {
        return side == white ? whitePieceCount : blackPieceCount;
    }


    public void incrementPieceCount(Piece.Side side) {
        if (side == white)
            whitePieceCount++;
        else
            blackPieceCount++;
        pieceCount++;
    }


    public void decrementPieceCount(Piece.Side side) {
        if (side == white)
            whitePieceCount--;
        else
            blackPieceCount--;
        pieceCount--;
    }


    public int getTableVersion() {
        if (pieceCount > 20)
            return 0;
        else if (pieceCount > 16)
            return 1;
        else
            return 2;
    }
}
