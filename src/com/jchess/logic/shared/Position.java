package com.jchess.logic.shared;


public class Position {

    public int row;
    public int col;

    public Position(int r, int c) {
        row = r;
        col = c;
    }

    public Position(Position p) {
        row = p.row;
        col = p.col;
    }

    public boolean equals(Object other) {
        if (other instanceof Position) {
            Position o = (Position) other;
            if (this.row == o.row && this.col == o.col)
                return true;
        }
        return false;
    }
}
