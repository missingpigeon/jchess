package com.jchess.logic.shared;

import com.jchess.logic.AI.Board;
import com.jchess.logic.AI.Piece;


public class Move implements Comparable<Move> {

    public Position source;
    public Position target;

    public Move extra;

    public Board board;


    public Move(Position src, Position tgt, Board board) {
        source = src;
        target = tgt;
        extra = null;
        this.board = board;
    }


    public Move(Position src, Position tgt, Move ex, Board board) {
        source = src;
        target = tgt;
        extra = ex;
        this.board = board;
    }


    public boolean equals(Object other) {
        if (other instanceof Move) {
            Move o = (Move) other;
            if (this.source.equals(o.source) && this.target.equals(o.target))
                return true;
        }
        return false;
    }


    public int compareTo(Move other) {
        int difference = 0;
        Piece currentTarget = board.getPiece(this.target);
        Piece otherTarget = board.getPiece(other.target);
        Piece currentSource = board.getPiece(this.source);
        Piece otherSource = board.getPiece(other.source);

        // If either moves are capture moves, take into consideration the difference between the
        // value of the capturing piece and the value of the piece to be captured
        if (currentTarget != null)
            difference += currentSource.getPieceValue() - currentTarget.getPieceValue();
        if (otherTarget != null)
            difference += otherTarget.getPieceValue() - otherSource.getPieceValue();

        // Take into consideration the change in position score moving from the source to the target
        if (currentSource.getSide() == Piece.Side.white) {
            difference += Board.pieceScores[board.getTableVersion()][currentSource.getPieceID()][this.source.row][this.source.col]
                    - Board.pieceScores[board.getTableVersion()][currentSource.getPieceID()][this.target.row][this.target.col];
        } else {
            difference += Board.pieceScores[board.getTableVersion()][currentSource.getPieceID()][7 - this.source.row][this.source.col]
                    - Board.pieceScores[board.getTableVersion()][currentSource.getPieceID()][7 - this.target.row][this.target.col];
        }

        return difference;
    }
}
