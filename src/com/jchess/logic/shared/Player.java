package com.jchess.logic.shared;

import com.jchess.logic.AI.Board;
import com.jchess.logic.AI.Piece;
import com.jchess.logic.AI.Queen;


public class Player {

    private Piece upgradePiece;
    private int status;
    private String name;
    private double totalTimeTaken;
    private int movesMade;
    private long startTime;
    private boolean isCurrentPlayer;
    protected Piece.Side side;
    protected int[] piecesCount;
    protected boolean isHuman;

    public static final int STATE_STALEMATE = 12;
    public static final int STATE_DRAW = 13;
    public static final int STATE_CHECKMATE = 14;
    public static final int STATE_LOST = 15;

    public Player(Piece.Side side) {
        // Default pawn promotions to queens
        upgradePiece = new Queen(side);
        this.side = side;
        piecesCount = new int[6];
        name = side == Piece.Side.white ? "White Player" : "Black Player";
        isHuman = true;
        totalTimeTaken = 0;
    }


    public Piece.Side getSide() {
        return side;
    }


    public void setSide(Piece.Side side) {
        this.side = side;
    }


    public Piece getPromotePiece() {
        return upgradePiece;
    }


    public void setPromotePiece(Piece piece) {
        upgradePiece = piece;
    }


    public String getName() {
        return name;
    }


    public Move getNextMove(Board b) {
        // This method is overridden in Computer
        return null;
    }


    public void incrementPieceCount(int piece) {
        piecesCount[piece]++;
    }


    public void decrementPieceCount(int piece) {
        piecesCount[piece]++;
    }


    public void resetPieceCount() {
        piecesCount = new int[6];
    }


    public int getPieceCount(int piece) {
        return piecesCount[piece];
    }

    public boolean isCurrentPlayer() {
        return isCurrentPlayer;
    }

    public void setCurrentPlayer(boolean currentPlayer) {
        isCurrentPlayer = currentPlayer;
    }


    public int getTotalTimeTaken() {
        return (int) totalTimeTaken;
    }


    public void startTimedMove() {
        startTime = System.currentTimeMillis();
    }


    public void endTimedMove() {
        long stop = System.currentTimeMillis();
        totalTimeTaken += (stop - startTime) / 1000.0;
    }


    public void incrementMovesMade() {
        movesMade++;
    }


    public int getMovesMade() {
        return movesMade;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
