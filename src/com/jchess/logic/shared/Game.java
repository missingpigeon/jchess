package com.jchess.logic.shared;

import java.util.Collection;
import java.util.LinkedList;

import com.jchess.logic.AI.*;
import com.jchess.logic.AI.Computer;


public class Game {

    public static Game currentGame;
    private Player white;
    private Player black;
    private Player computerPlayer;
    private Piece.Side lastMoved;
    private Move lastMoveChecked;
    private Board board;
    private int gameType;
    private Piece.Side winner;
    private int gamestate;
    private Piece.Side computerSide;
    private LinkedList<Board> previousBoardStates;
    public static final int GAMEMODE_SINGLEPLAYER = 0;
    public static final int GAMEMODE_MULTIPLAYER = 1;
    public static final int MOVE_SUCCESS = 0;
    public static final int MOVE_CHECKMATE = 1;
    public static final int MOVE_WRONG_TURN = 2;
    public static final int MOVE_ERROR = 3;
    public static final int GAME_SERVER = 2;
    public static final int GAME_CLIENT = 3;
    public static final int DRAW_STALEMATE = 22;
    public static final int DRAW_THREEFOLD = 23;
    public static final int DRAW_FIFTYMOVE = 24;
    public static final int DRAW_IMPOSSIBLE = 25;

    public Position getPosition(String code) {
        return board.getPosition(code);
    }

    public Piece getPiece(Position pi) {
        return board.getPiece(pi);
    }

    public void setLastMoved(Piece.Side side) {
        lastMoved = side;
    }

    public Game(int gameMode) {
        white = new Player(Piece.Side.white);
        board = new Board();
        lastMoved = null;
        winner = null;
        currentGame = this;
        gameType = gameMode;
        previousBoardStates = new LinkedList<Board>();

        if (gameMode == GAMEMODE_SINGLEPLAYER) {
            black = computerPlayer = new Computer(Piece.Side.black, 4);
            lastMoved = computerSide = Piece.Side.black;
        } else {
            lastMoved = Piece.Side.black;
            black = new Player(Piece.Side.black);
            computerPlayer = null;
            computerSide = null;
        }
        updatePieceCounts();
        updatePlayerStatus();
        white.startTimedMove();
    }


    private void updatePieceCounts() {

        white.resetPieceCount();
        black.resetPieceCount();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Piece piece = board.getPiece(i, j);
                if (piece != null) {
                    getPlayer(piece.getSide()).incrementPieceCount(piece.getPieceID());
                }
            }
        }
    }


    private void updatePlayerStatus() {

        if (gamestate == Game.DRAW_STALEMATE) {
            white.setStatus(Player.STATE_STALEMATE);
            black.setStatus(Player.STATE_STALEMATE);
        } else if (gamestate > DRAW_STALEMATE) {
            white.setStatus(Player.STATE_DRAW);
            black.setStatus(Player.STATE_DRAW);
        } else {
            if (winner == null) {

                if (lastMoved == Piece.Side.white) {
                    white.setCurrentPlayer(false);
                    black.setCurrentPlayer(true);
                } else {
                    white.setCurrentPlayer(true);
                    black.setCurrentPlayer(false);
                }
            } else if (winner == Piece.Side.white) {

                white.setStatus(Player.STATE_CHECKMATE);;
                black.setStatus(Player.STATE_LOST);
            } else if (winner == Piece.Side.black) {
                white.setStatus(Player.STATE_LOST);
                black.setStatus(Player.STATE_CHECKMATE);
            }
        }
    }


    public Computer getComputerPlayer() {
        return (Computer) computerPlayer;
    }


    public Player getPlayer(Piece.Side side) {
        return side == Piece.Side.white ? white : black;
    }


    public void makeMove(Move move) {

        if (lastMoveChecked != null) {
            move = lastMoveChecked;
            previousBoardStates.add(new Board(board));
        }

        lastMoved = board.getPiece(move.source).getSide();

        board.makeMove(move);

        winner = board.checkForWins(lastMoved);

        gamestate = board.checkForDraw();

        int piecesCount = board.getPiecesCount();
        if (winner == null) {
            if (board.getConsecPassiveMovesCount() >= 50) {
                gamestate = Game.DRAW_FIFTYMOVE;
            } else if (piecesCount == 2) {
                gamestate = Game.DRAW_IMPOSSIBLE;
            } else if (gameType == Game.GAMEMODE_SINGLEPLAYER) {
                int i = previousBoardStates.size() - 1;
                if (i >= 3 && board.equals(previousBoardStates.get(i - 1)) && board.equals(previousBoardStates.get(i - 3)))
                    gamestate = Game.DRAW_THREEFOLD;
            } else {
                int i = previousBoardStates.size() - 1;
                if (i >= 7 && board.equals(previousBoardStates.get(i - 3)) && board.equals(previousBoardStates.get(i - 7)))
                    gamestate = Game.DRAW_THREEFOLD;
            }
        }

        if (winner == null) {
            if (piecesCount == 3 || piecesCount == 4) {
                int bishopCount = 0;
                int knightCount = 0;
                for (int r = 0; r < 8; r++) {
                    for (int c = 0; c < 8; c++) {
                        if (board.getPiece(r, c) instanceof Bishop)
                            bishopCount++;
                        else if (board.getPiece(r, c) instanceof Knight)
                            knightCount++;
                    }
                }
                if ((piecesCount == 3 && bishopCount + knightCount == 1) || knightCount == 2)
                    gamestate = Game.DRAW_IMPOSSIBLE;
            }

            lastMoveChecked = null;
        }

        updatePieceCounts();
        updatePlayerStatus();
    }


    public int canMakeMove(Move move) {

        Piece.Side playerSide = board.getPiece(move.source).getSide();
        Collection<Move> moves = board.getPiece(move.source).generateLegalMoves(board, move.source);

        for (Move m : moves) {
            if (m.equals(move)) {
                if (lastMoved == playerSide)
                    return MOVE_WRONG_TURN;

                Board b = new Board(board);
                b.makeMove(m);
                King king;
                Position pos;

                if (playerSide == Piece.Side.white)
                    pos = b.getWhiteKingPos();
                else
                    pos = b.getBlackKingPos();
                king = (King) b.getPiece(pos);

                if (king.isInCheck(b, pos))
                    return MOVE_CHECKMATE;

                lastMoveChecked = m;
                return MOVE_SUCCESS;
            }
        }
        return MOVE_ERROR;
    }


    public Collection<Move> getLegalMoves(Position piece) {
        return board.getPiece(piece).generateLegalMoves(board, piece);
    }


    public Board getGameState() {
        return board;
    }


    public Piece.Side getWinner() {
        return winner;
    }


    public int getStateGameDraw() {
        return gamestate;
    }


    public Piece.Side getLastMoved() {
        return lastMoved;
    }
}